// 1. Reverse a string.
const reverseString = text => text.split("").reverse().join("");
console.assert(reverseString("Michelle") === "ellehciM", "Kata 1A");
console.assert(reverseString("Lala") === "alaL", "Kata 1B");

// 2. Reverse a sentence ("bob likes dogs" -> "dogs likes bob").
const reverseSentence = text => text.split(" ").reverse().join(" ");
console.assert(reverseSentence("Michelle eats poprocks.") === "poprocks. eats Michelle", "Kata 2A");
console.assert(reverseSentence("Boo bear") === "bear Boo", "Kata 2B");

// 3. Find the minimum value in an array.
const findLowestNumber = array => Math.min(...array);
console.assert(findLowestNumber([4, -20]) === -20, "Kata 3A");
console.assert(findLowestNumber([4, 6]) === 4, "Kata 3B");

// 4. Find the maximum value in an array.
const findHighestNumber = array => Math.max(...array);
console.assert(findHighestNumber([4, -20]) === 4, "Kata 4A");
console.assert(findHighestNumber([4, 6]) === 6, "Kata 4B");

// 5. Calculate a remainder (given a numerator and denominator).
const findRemainder = (numerator, denominator) => numerator % denominator;
console.assert(findRemainder(12, 5) === 2, "Kata 5A");
console.assert(findRemainder(12, 6) === 0, "Kata 5B");

// 6. Return distinct values from a list including duplicates (i.e. "1 3 5 3 7 3 1 1 5" -> "1 3 5 7").

// array for num 6 and 7
// const duplicatesArray = [1, 3, 5, 3, 7, 3, 1, 1, 5];
// const uniqueItemsFromDuplicatesArray = [...new Set(duplicatesArray)];

// const findUniqueItems = array => [...new Set(array)];
// console.assert(findUniqueItems([1, 3, 5, 3, 7, 3, 1, 1, 5]) === [1, 3, 5, 7], "Kata 6A")
// console.log(findUniqueItems([1, 3, 5, 3, 7, 3, 1, 1, 5]))
// console.assert(findUniqueItems([1, 3, 5, 3, 7, 3, 1, 1, 5]) === [1, 3], "Kata 6B")

// 7. Return distinct values and their counts (i.e. the list above becomes "1(3) 3(3) 5(2) 7(1)").
function number7() {
    function getCharCount(charCount, char) {
        charCount[char] ? charCount[char] += 1 : charCount[char] = 1;
        return charCount;
    }
    let duplicatesCount = duplicatesArray.reduce(getCharCount, []);
    for (let i = 0; i < duplicatesCount.length; i++) {
        if (duplicatesCount[uniqueItemsFromDuplicatesArray[i]] > 0) {
            let text7 = uniqueItemsFromDuplicatesArray[i] + "(" + duplicatesCount[uniqueItemsFromDuplicatesArray[i]] + ")" + " ";
        }
    }
}

// 8. Given a string of expressions (only variables, +, and -) and an object describing a set of variable/value pairs like {a: 1, b: 7, c: 3, d: 14}, return the result of the expression ("a + b+c -d" would be -3).
const findExpressionResult = (a, b, c, d) => a + b + c - d;
console.assert(findExpressionResult(1, 7, 3, 14) === -3, "Kata 8A");
console.assert(findExpressionResult(1, 7, 3, 10) === 1, "Kata 8B");